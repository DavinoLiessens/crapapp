import React from "react";
import UserProfile from "../../shared/components/userProfile";


const UserListItem = (props) => {
    return (
        <div className="row">
            <table className="col s12 searchTable">
                <tbody className="col s12">
                    <tr className="col s12">
                        <td className="col s5">
                            <UserProfile />
                        </td>
                        <td className="col s4 buttonCell">
                            <a className="waves-effect waves-light btn">ADD FRIEND</a>
                        </td>
                        <td className="col s2 buttonCell">
                            <a className="waves-effect waves-light btn redButton">IGNORE</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

UserListItem.propTypes = {

}

export default UserListItem;