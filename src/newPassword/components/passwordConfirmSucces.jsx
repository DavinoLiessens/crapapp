import React, {Component, Fragment} from 'react';
import Banner from "../../shared/assets/crapappbanner.png";
import {regex, routes} from '../../config';
import {Redirect} from "react-router-dom";
import { FieldControl, FieldGroup, FormBuilder, Validators } from 'react-reactive-form';
import {newPassword} from "../../shared/services/newPassword";
import {checkIfMatchingPassword} from "../../shared/services/password";
import PasswordInput from "../../shared/components/passwordInput";

class PasswordConfirmSucces extends Component {
    getMailForm = FormBuilder.group({
        password: ['', Validators.compose([Validators.required,
            Validators.pattern(regex.password)])],
        passwordConfirm: ['', Validators.compose([
            Validators.required,
            Validators.pattern(regex.password)
        ])]
    },
        { validators: checkIfMatchingPassword('password', 'passwordConfirm') }
    );

    constructor(props) {
        super(props);

        this.state = {
            redirection: false
        }
    }

    render() {
        return (
            this.state.redirection ? <Redirect to={routes.confirmProfileDataUpdate}/> :
                <Fragment>
                    <FieldGroup
                        control={this.getMailForm}
                        render={({invalid}) => (
                            <form>
                                <div className="container">
                                    <div className="row">
                                        <div className="col s8 offset-s2">
                                            <div className="card">
                                                <div className="card-image">
                                                    <img src={Banner} className="banner" alt="logo crap app"/>
                                                </div>
                                                <div className="card-content">
                                                    <div className="row">
                                                        <FieldControl
                                                            name="password"
                                                            render={PasswordInput}
                                                            meta={{label: "password"}}
                                                        />
                                                        <FieldControl
                                                            name="passwordConfirm"
                                                            render={PasswordInput}
                                                            meta={{label: "passwordConfirm"}}
                                                        />
                                                    </div>
                                                    <div className="card-action">
                                                        <a className="waves-effect waves-light btn"
                                                           disabled={invalid}
                                                           onClick={() => this.setPassword()}
                                                           id="newPasswordButton"
                                                        >send new password</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        )}/>
                </Fragment>
        );
    }

    setPassword() {
        newPassword(this.getMailForm.value).then((response) => {
            if (response.status === 200) {
                this.setState({redirection: true});
            } else if (response.status === 400) {
                alert('Your new password can not be the same as your old password');
            }
        });
    }
}

export default PasswordConfirmSucces;
