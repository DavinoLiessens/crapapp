import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import PlayingFieldRowComputer from './playingFieldRowComputer';

const PlayingFieldComputer = (props) => {
    return (
        <Fragment>
            <div className="col s6 gameCard">
                <div className="card">
                    <div className="card-content">
                        <div className="row">
                            <h7 className="col s3 offset-s5">{props.playerName}</h7>
                        </div>
                        <div className="row">
                            <div className="col s1 offset-s2">A</div>
                            <div className="col s1">B</div>
                            <div className="col s1">C</div>
                            <div className="col s1">D</div>
                            <div className="col s1">E</div>
                            <div className="col s1">F</div>
                            <div className="col s1">G</div>
                            <div className="col s1">H</div>
                            <div className="col s1">I</div>
                            <div className="col s1">J</div>
                        </div>
                        <div className="row">
                            <Fragment>
                                <PlayingFieldRowComputer index={1} gameId={props.gameId} />
                                <PlayingFieldRowComputer index={2} gameId={props.gameId} />
                                <PlayingFieldRowComputer index={3} gameId={props.gameId} />
                                <PlayingFieldRowComputer index={4} gameId={props.gameId} />
                                <PlayingFieldRowComputer index={5} gameId={props.gameId} />
                                <PlayingFieldRowComputer index={6} gameId={props.gameId} />
                                <PlayingFieldRowComputer index={7} gameId={props.gameId} />
                                <PlayingFieldRowComputer index={8} gameId={props.gameId} />
                                <PlayingFieldRowComputer index={9} gameId={props.gameId} />
                                <PlayingFieldRowComputer index={10} gameId={props.gameId} />
                            </Fragment>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment >
    );
}

PlayingFieldComputer.propTypes = {
    playerName: PropTypes.string,
    gameId: PropTypes.number
}

export default PlayingFieldComputer;