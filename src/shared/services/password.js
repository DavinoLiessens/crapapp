import { SHA3 } from 'sha3';

  export function hashPassword(password) {
    const hash = new SHA3(512);
 
    hash.update(password);

   let result = hash.digest('hex');
  
   hash.reset();
    return result
}

 export function checkIfMatchingPassword (passwordKey, passwordConfirmationKey) {
  return (group) => {
    let passwordInput = group.controls[passwordKey],
      passwordConfirmationInput = group.controls[passwordConfirmationKey];
    if (passwordInput.value !== passwordConfirmationInput.value) {
      passwordConfirmationInput.setErrors({ notEquivalent: true });
    } else {
      passwordConfirmationInput.setErrors(null);
    }
    return null;
  };
};